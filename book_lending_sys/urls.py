from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/accounts/', include('accounts.urls')),
    path('api/repo/', include('library_repo.urls')),
    path('api/lending/', include('lending.urls')),
]
