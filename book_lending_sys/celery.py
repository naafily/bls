import os
from celery import Celery
from book_lending_sys import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'book_lending_sys.settings')

app = Celery('book_lending_sys', broker=settings.CELERY_BROKER_URL, include=["notifications.tasks"])
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
