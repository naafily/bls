from django.contrib import admin

from .models import Book, Label, Author, Category

admin.site.register(Book)
admin.site.register(Label)
admin.site.register(Author)
admin.site.register(Category)
