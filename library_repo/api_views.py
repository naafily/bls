from django.db.models import Q
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework import generics

from .models import Book
from .permissions import BookCRUDPermission
from .serializers import BookSerializer


class BookCreate(generics.CreateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (BookCRUDPermission,)


class BookList(generics.ListAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    filterset_fields = '__all__'
    search_fields = '__all__'
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]


class BookDetail(generics.RetrieveAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class BookChange(generics.UpdateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (BookCRUDPermission,)


class BookDestroy(generics.DestroyAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (BookCRUDPermission,)


@method_decorator(cache_page(60 * 15), name='dispatch')
class SameBookList(generics.ListAPIView):
    serializer_class = BookSerializer

    def get_queryset(self):
        queries = Q(labels__books__isbn=self.kwargs['pk']) | Q(category__books__isbn=self.kwargs['pk'])
        return Book.objects.filter(queries).exclude(isbn=self.kwargs['pk']).distinct()
