from django.apps import AppConfig


class LibraryRepoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'library_repo'
