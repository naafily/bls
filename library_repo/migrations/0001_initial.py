# Generated by Django 3.2.5 on 2021-07-08 12:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Label',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('title', models.CharField(max_length=50)),
                ('isbn', models.CharField(max_length=50, primary_key=True, serialize=False)),
                ('copies_num', models.PositiveSmallIntegerField()),
                ('max_lend_days', models.PositiveSmallIntegerField()),
                ('author', models.ManyToManyField(related_name='books', to='library_repo.Author')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='books', to='library_repo.category')),
                ('labels', models.ManyToManyField(to='library_repo.Label')),
            ],
        ),
    ]
