from django.urls import path

from .api_views import BookList, BookChange, BookCreate, BookDestroy, BookDetail, SameBookList

urlpatterns = [
    path('books/', BookList.as_view()),
    path('books/<int:pk>', BookDetail.as_view()),
    path('books/create/', BookCreate.as_view()),
    path('books/destroy/<int:pk>', BookDestroy.as_view()),
    path('books/change/<int:pk>', BookChange.as_view()),
    path('books/similar/<int:pk>', SameBookList.as_view()),

]
