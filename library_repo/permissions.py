from rest_framework.permissions import BasePermission

from accounts.models import Manager


class BookCRUDPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return Manager.objects.filter(user=request.user).first()

    def has_object_permission(self, request, view, obj):
        return Manager.objects.filter(user=request.user).exists()
