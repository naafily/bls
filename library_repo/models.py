from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=50)


class Label(models.Model):
    name = models.CharField(max_length=50)


class Category(models.Model):
    name = models.CharField(max_length=50)


class Book(models.Model):
    title = models.CharField(max_length=50)
    isbn = models.CharField(primary_key=True, max_length=50)
    author = models.ManyToManyField(Author, related_name='books')
    category = models.ManyToManyField(Category, related_name='books')
    labels = models.ManyToManyField(Label, related_name='books')
    total_num = models.PositiveSmallIntegerField()
    available_num = models.PositiveSmallIntegerField()
    max_lend_days = models.PositiveSmallIntegerField()
    is_notified = models.BooleanField(default=False)

    def decrease_available_num(self):
        self.available_num = self.available_num - 1
        self.save(update_fields=['available_num'])

    def increase_available_num(self):
        self.available_num = self.available_num + 1
        self.save(update_fields=['available_num'])

    def is_available(self):
        return self.available_num > 0
