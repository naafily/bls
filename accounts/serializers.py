from rest_framework import serializers

from .models import Manager, Student


class ManagerLoginSerializer(serializers.ModelSerializer):
    user_id = serializers.CharField(source='user.id', write_only=True,
                                    required=True)
    user_password = serializers.CharField(source='user.password', write_only=True,
                                          required=True)

    class Meta:
        model = Manager
        fields = ('user_id', 'user_password')


class StudentLoginSerializer(serializers.ModelSerializer):
    user_id = serializers.CharField(source='user.id', write_only=True,
                                    required=True)
    user_password = serializers.CharField(source='user.password', write_only=True,
                                          required=True)

    class Meta:
        model = Student
        fields = ('user_id', 'user_password')
