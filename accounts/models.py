from django.contrib.auth.models import User
from django.db import models

from lending.models import Debt, Borrow


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='student')

    def has_debt(self):
        debt = Debt.objects.filter(borrow__student=self, payed_at=None,
                                   borrow__returned_date__isnull=False).first()
        if debt is not None:
            return debt
        return False

    def must_return_book(self):
        borrow = Borrow.objects.filter(student=self, returned_date=None).first()
        if borrow is not None:
            return borrow
        return False


class Manager(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='manager')
