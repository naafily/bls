from django.urls import path
from .api_views import ManagerLoginAPIView, StudentLoginAPIView

urlpatterns = [
    path('mangers/login/', ManagerLoginAPIView.as_view()),
    path('students/login/', StudentLoginAPIView.as_view()),
]
