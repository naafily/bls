from django.contrib import admin

from .models import Manager, Student

admin.site.register(Student)
admin.site.register(Manager)
