from django.contrib.auth import authenticate, login
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Manager, Student
from .serializers import StudentLoginSerializer, ManagerLoginSerializer


class BaseLoginAPIView(APIView):
    def _get_model_class(self):
        raise NotImplementedError()

    def post(self, request):
        user_id = request.data.get('user_id')
        password = request.data.get('user_password')
        model = self._get_model_class()
        obj = get_object_or_404(model, user_id=user_id)
        user = obj.user
        if user is not None:
            authenticated_user = authenticate(username=user.username, password=password)
            if authenticated_user is not None:
                login(request, user)
                return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)


class ManagerLoginAPIView(BaseLoginAPIView):
    serializer_class = ManagerLoginSerializer

    def _get_model_class(self):
        return Manager


class StudentLoginAPIView(BaseLoginAPIView):
    serializer_class = StudentLoginSerializer

    def _get_model_class(self):
        return Student
