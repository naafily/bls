from celery import shared_task
from django.core.mail import send_mail

from accounts.models import Student
from book_lending_sys import settings
from library_repo.models import Book

_SUBJECT = "NEW BOOKS IN LIBRARY"
from_email = settings.EMAIL_HOST_USER


@shared_task
def send_email_new_books():
    recievers = []
    book_names = ""
    students = Student.objects.all()
    for student in students:
        recievers.append(student.user.email)

    books = Book.objects.all()
    for book in books:
        if not book.is_notified:
            book_names = book_names + str(book.title) + '\n'
            book.is_notified = True
            book.save(update_fields=['is_notified'])

    return send_mail(_SUBJECT, book_names, from_email, recievers)
