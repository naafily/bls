from datetime import timedelta

from django.contrib.auth.models import User
from django.db import transaction
from django.utils import timezone
from rest_framework import views, status
from rest_framework.response import Response

from .models import Debt
from .permissions import BorrowPermission
from .serializers import BorrowSerializer


class BorrowRequestView(views.APIView):
    serializer_class = BorrowSerializer
    permission_classes = (BorrowPermission,)

    @transaction.atomic
    def post(self, request):
        student = request.user.student
        borrow_allowed = student.has_debt() is False and student.must_return_book() is False
        if borrow_allowed:
            serializer = BorrowSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            book = serializer.validated_data['book']
            if book.is_available():
                book.decrease_available_num()
                serializer.save(
                    to_be_returned_date=serializer.validated_data['lend_date'] + timedelta(days=book.max_lend_days))
            return Response('Created successfully', status=status.HTTP_201_CREATED)
        return Response('Denied', status=status.HTTP_403_FORBIDDEN)


class ReturnBookView(views.APIView):
    permission_classes = (BorrowPermission,)

    @transaction.atomic
    def post(self, request):
        student = request.user.student
        borrow = student.must_return_book()
        if borrow:
            borrow.returned_date = timezone.now()
            borrow.save(update_fields=['returned_date'])
            borrow.book.increase_available_num()
            if borrow.has_delay():
                amount = borrow.get_delayed_days_count()
                Debt.objects.create(borrow=borrow, amount=amount.days)
            return Response("Updated successfully", status=status.HTTP_200_OK)
        return Response("Not Found", status=status.HTTP_404_NOT_FOUND)


class PayDebtView(views.APIView):
    permission_classes = (BorrowPermission,)

    @transaction.atomic
    def post(self, request):
        user = User.objects.get(id=request.user.id)
        student = user.student
        debt = student.has_debt()
        if debt:
            debt.payed_at = timezone.now()
            debt.save(update_fields=['payed_at'])
            return Response('Updated successfully', status=status.HTTP_200_OK)
        return Response('Not Found', status=status.HTTP_404_NOT_FOUND)
