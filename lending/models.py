from django.db import models

from library_repo.models import Book


class Borrow(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name='borrows')
    student = models.ForeignKey('accounts.Student', on_delete=models.CASCADE, related_name='borrows')
    lend_date = models.DateTimeField()
    to_be_returned_date = models.DateTimeField()
    returned_date = models.DateTimeField(null=True)

    def has_delay(self):
        return self.returned_date > self.to_be_returned_date

    def get_delayed_days_count(self):
        d = self.returned_date - self.to_be_returned_date
        return d


class Debt(models.Model):
    borrow = models.ForeignKey(Borrow, on_delete=models.CASCADE, related_name='debts')
    amount = models.PositiveIntegerField()
    payed_at = models.DateTimeField(null=True)
