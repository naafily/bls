from django.contrib import admin

from .models import Borrow, Debt

admin.site.register(Borrow)
admin.site.register(Debt)
