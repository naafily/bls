from rest_framework.permissions import BasePermission

from accounts.models import Student


class BorrowPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return Student.objects.filter(user=request.user).exists()

    def has_object_permission(self, request, view, obj):
        return Student.objects.filter(user=request.user).exists()
