from django.urls import path

from .api_views import BorrowRequestView, ReturnBookView, PayDebtView

urlpatterns = [
    path('borrow/', BorrowRequestView.as_view()),
    path('return-book/', ReturnBookView.as_view()),
    path('pay-debt/', PayDebtView.as_view()),
]
